# Rave Hula Hoop

Here is a script to liven up the Purr & Chase hula hoop tail toy. 

It changes colors and is bright and glowing.

## Instructions

1. Create a new script inside the hula hoop and copy/paste the contents of this file inside. 
2. Edit to taste if you like.
3. Save the script and enjoy.

## Gotchas

This uses Firestorm's LSL extensions. I don't know if this will work on other viewers.
