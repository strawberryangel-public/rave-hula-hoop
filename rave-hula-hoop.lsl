// How many seconds delay between color changes.
#define DELAY 1.0

// How much glow to give. 
// This is a number between 0.0 (no glow)
// and 1.0 (maximum glow). 
// Generally this should be 0.1 to 0.2.
#define GLOW 0.15

// How bright to make the light
// This is a value between 0.0 (off)
// and 1.0 (fullly bright).
#define BRIGHTNESS 0.6

// -------- Don't change things below unless you know what you're doing. ---------

// The hoop is on this link.
#define LINK 2

list colors = [
<1,0,0>, // Red
<1,0.5,0>,  // Orange
<1,1,0>,  // Yellow
<0,1,0>, // Green
<0,0,1>, // Blue
<0,1,1>, // Cyan
<1,0,1> // Magenta
];

integer colorCount;
vector currentColor = ZERO_VECTOR;

vector chooseColor()
{
    integer idx = llFloor(llFrand(colorCount));
    return llList2Vector(colors, idx);
}

changeColor()
{
    vector newColor = currentColor;
    while(newColor == currentColor) {
        newColor = chooseColor();
    }
    currentColor = newColor;

    llSetLinkPrimitiveParamsFast(LINK, [
        PRIM_POINT_LIGHT, 1, currentColor, BRIGHTNESS, 1.5, 2.0,
        PRIM_COLOR, ALL_SIDES, currentColor, 1
    ]);
}

default
{
    state_entry()
    {
        colorCount = llGetListLength(colors);
        llSetTimerEvent(DELAY);

        // Set these once.
        llSetLinkPrimitiveParamsFast(LINK, [
            PRIM_GLOW, ALL_SIDES, GLOW,
            PRIM_FULLBRIGHT, ALL_SIDES, 1
        ]);
    }

    timer()
    {
        changeColor();
    }
}
